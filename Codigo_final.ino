/* PROJETO DE SIMULAÇÃO DE CASA AUTOMATICA
 * EQUIPE: JOSE EVANDRO DE ALMEIDA
 *         ADRYEL DA SILVA NAJAR
 *         NÚBIA SERIQUE
 */

/*Definindo constante para o ID do template criado no app blynk, dando acesso a esp32
 *Definindo constante com o nome do arquivo criado no app blynk
 *Definindo constante para o TOKEN de autentificação para blynk se comunicar com a esp32
 */
#define BLYNK_TEMPLATE_ID           "TMPLoNyFzfvV"
#define BLYNK_DEVICE_NAME           "Quickstart Device"
#define BLYNK_AUTH_TOKEN            "Hgh3QpR5IJUkucYvafLS63qkEFLz8uVj"

//Comando para imprimir no monitor serial, o Debug do blynk
#define BLYNK_PRINT Serial

//Incluindo as bibliotecas dos wifi para conexar a esp32 na internet e no servidor do blynk
#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>

//Definindo constantes para os pinos dos componentes do circuito
#define led_q1 26
#define led_q2 2
#define led_q3 12
#define led_q4 27
#define led1_ldr 4
#define led2_ldr 15 
#define led3_ldr 13 
#define sensor_ldr 35
#define sensor_mag1 34
#define sensor_mag2 14
#define pwm 32
#define pin1_motor 33
#define pin2_motor 25

//Passando as credenciais para a conexão WiFi
char auth[] = BLYNK_AUTH_TOKEN;
char ssid[] = "Fala_Mansa";
char pass[] = "05694546202";

//Criando variaveis para controle da velocidade do motor por pwm
int vel=0,value;
const int freq = 3000;
const int pwmCanal = 0;
const int resolucao = 8;

//Definindo um objeto de BlynkTimer, para a contagem do tempo do programa
BlynkTimer timer;

//Função chamada toda vez que o valor do pino Virtual 0 (V0) que controla led_q1 é alterado.
BLYNK_WRITE(V0)
{
  value = param.asInt();
  digitalWrite(led_q1,value);
}

//Função chamada toda vez que o valor do pino Virtual 1 (V1) que controla o valor da velocidade do motor é alterado
BLYNK_WRITE(V1){
  vel = param.asInt();
  ledcWrite(pwmCanal,vel);
  Blynk.virtualWrite(V3,vel); 
}

//Função chamada toda vez que o valor do pino Virtual 4 (V4) que controla led_q2 é alterado.
BLYNK_WRITE(V4)
{
  value = param.asInt();
  digitalWrite(led_q2,value);
}

//Função chamada toda vez que o valor do pino Virtual 5 (V5) que controla led_q3 é alterado.
BLYNK_WRITE(V5)
{
  value = param.asInt();
  digitalWrite(led_q3,value);
}

//Função chamada toda vez que o valor do pino Virtual 6 (V6) que controla led_q44 é alterado.
BLYNK_WRITE(V6)
{
  value = param.asInt();
  digitalWrite(led_q4,value);
}

//Função chamada toda vez que o valor do pino Virtual 7 (V0) que controla o motor é alterado.
BLYNK_WRITE(V7)
{
  value = param.asInt();
  //Abre o portão
  if(value == 1){
    do{
      digitalWrite(pin1_motor,HIGH);
      digitalWrite(pin2_motor,LOW);
    }while(digitalRead(sensor_mag1)!=HIGH);
  }else{
    //Fecha o portão
    do{
      digitalWrite(pin1_motor,LOW);
      digitalWrite(pin2_motor,HIGH);
    }while(digitalRead(sensor_mag2)!=HIGH);
  }
  parar_motor();
}

//Função que mostra o tempo corrido da programação
void myTimerEvent()
{
  Blynk.virtualWrite(V2, millis() / 1000);
}

//Primeira função a ser realizada, definindo o comportamento dos pinos como INPUT ou OUTPUT
void setup()
{
  pinMode(sensor_ldr,INPUT);
  pinMode(sensor_mag2,INPUT);
  pinMode(sensor_mag1,INPUT);
  pinMode(pwm,OUTPUT);
  pinMode(pin1_motor,OUTPUT);
  pinMode(pin2_motor,OUTPUT);
  pinMode(led_q1,OUTPUT);
  pinMode(led_q2,OUTPUT);
  pinMode(led_q3,OUTPUT);
  pinMode(led_q4,OUTPUT);
  pinMode(led1_ldr,OUTPUT);
  pinMode(led2_ldr,OUTPUT);
  pinMode(led3_ldr,OUTPUT);
  Serial.begin(115200); //Iniciado o monitor Serial
  
  ledcSetup(pwmCanal, freq, resolucao); //Comando que defini a frequencia e a resolução do canal do pwm
  ledcAttachPin(pwm,pwmCanal); //Comando que inseri o pino do pwm no canal do pwm
  ledcWrite(pwmCanal,vel); //Comando que inicia um valor 0 no canal do pwm
  
  Blynk.begin(auth, ssid, pass); // inicia a conexão do wifi do app blynk e esp32 com WiFi conectado com o Blynk Cloud
  timer.setInterval(1000L, myTimerEvent); //Configurando para os dados serem enviados a cada segundo  para o Blynk Cloud
}

//Função que irá sempre se repetir
void loop()
{
  Blynk.run(); //Inicia o processo do Blynk
  timer.run(); //Inicia o processo do temporizador

  //Sistema de iluminosidade, que ao detectar pouca luz acende os leds
  if(digitalRead(sensor_ldr) == HIGH){
    digitalWrite(led1_ldr,HIGH);
    digitalWrite(led2_ldr,HIGH);
    digitalWrite(led3_ldr,HIGH);
  }else{
    digitalWrite(led1_ldr,LOW);
    digitalWrite(led2_ldr,LOW);
    digitalWrite(led3_ldr,LOW);
  }
} 

//Função chamada toda vez que o portão chega no fim do curso
void parar_motor()
{
  digitalWrite(pin1_motor,LOW);
  digitalWrite(pin2_motor,LOW);
  delay(2000);
}
